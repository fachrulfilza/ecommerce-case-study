<?php
defined('BASEPATH') OR EXIT('No direct access allowed');

class Product_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function getAllData()
	{
		return $this->db->get('products')->result();
	}
	
	function getProductById($id)
	{
        $this->db->where('id', $id);
        
		return $this->db->get('products')->row();
	}
	
}