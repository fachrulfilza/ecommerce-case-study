<!-- JS Library-->
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery/dist/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-bar-rating/dist/jquery.barrating.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/owl-carousel/owl.carousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/gmap3.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/imagesloaded.pkgd.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/isotope.pkgd.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery.matchHeight-min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/slick/slick/slick.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/elevatezoom/jquery.elevatezoom.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/Magnific-Popup/dist/jquery.magnific-popup.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/jquery.themepunch.tools.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/jquery.themepunch.revolution.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/extensions/revolution.extension.video.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/extensions/revolution.extension.navigation.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/extensions/revolution.extension.parallax.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/revolution/js/extensions/revolution.extension.actions.min.js'); ?>"></script>
<!-- Custom scripts-->
<script type="text/javascript" src="<?php echo base_url('assets/js/main.js'); ?>"></script>