<!DOCTYPE html>
<html lang="en">

	<?php $this->load->view('partials/head'); ?>

<body>
    <?php $this->load->view('partials/header'); ?>
    <main class="ps-main">
      <div class="test">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
            </div>
          </div>
        </div>
      </div>
      <div class="ps-product--detail pt-60">
        <div class="ps-container">
          <div class="row">
            <div class="col-lg-10 col-md-12 col-lg-offset-1">
              <div class="ps-product__thumbnail">
                <div class="ps-product__preview">
                  <div class="ps-product__variants">
                    <div class="item"><img src="<?php echo base_url('assets/images/shoe-detail/1.jpg'); ?>" alt=""></div>
                    <div class="item"><img src="<?php echo base_url('assets/images/shoe-detail/2.jpg'); ?>" alt=""></div>
                    <div class="item"><img src="<?php echo base_url('assets/images/shoe-detail/3.jpg'); ?>" alt=""></div>
                    <div class="item"><img src="<?php echo base_url('assets/images/shoe-detail/3.jpg'); ?>" alt=""></div>
                    <div class="item"><img src="<?php echo base_url('assets/images/shoe-detail/3.jpg'); ?>" alt=""></div>
                  </div>
                </div>
                <div class="ps-product__image">
                  <div class="item"><img class="zoom" src="<?php echo base_url('assets/images/shoe-detail/1.jpg'); ?>" alt="" data-zoom-image="<?php echo base_url('assets/images/shoe-detail/1.jpg'); ?>"></div>
                  <div class="item"><img class="zoom" src="<?php echo base_url('assets/images/shoe-detail/2.jpg'); ?>" alt="" data-zoom-image="<?php echo base_url('assets/images/shoe-detail/2.jpg'); ?>"></div>
                  <div class="item"><img class="zoom" src="<?php echo base_url('assets/images/shoe-detail/3.jpg'); ?>" alt="" data-zoom-image="<?php echo base_url('assets/images/shoe-detail/3.jpg'); ?>"></div>
                </div>
              </div>
              <div class="ps-product__thumbnail--mobile">
                <div class="ps-product__main-img"><img src="<?php echo base_url('assets/images/shoe-detail/1.jpg'); ?>" alt=""></div>
                <div class="ps-product__preview owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="20" data-owl-nav="true" data-owl-dots="false" data-owl-item="3" data-owl-item-xs="3" data-owl-item-sm="3" data-owl-item-md="3" data-owl-item-lg="3" data-owl-duration="1000" data-owl-mousedrag="on"><img src="images/shoe-detail/1.jpg" alt=""><img src="images/shoe-detail/2.jpg" alt=""><img src="images/shoe-detail/3.jpg" alt=""></div>
              </div>
              <div class="ps-product__info">
                <div class="ps-product__rating">
                  <select class="ps-rating">
                    <option value="1">1</option>
                    <option value="1">2</option>
                    <option value="1">3</option>
                    <option value="1">4</option>
                    <option value="2">5</option>
                  </select>
                </div>
                <h1><?php echo $product->nama; ?></h1>
                <h3 class="ps-product__price"><?php echo 'Rp'.number_format($product->harga,0,',','.'); ?></h3>
                <div class="ps-product__block ps-product__quickview">
                  <h4>Description</h4>
                  <p><?php echo $product->catatan; ?></p>
                </div>
                <div class="ps-product__block ps-product__size">
                  <h4>Quantity</h4>
                  <div class="form-group">
                    <input class="form-control" type="number" value="1">
                  </div>
                </div>
                <div class="ps-product__shopping"><a class="ps-btn mb-10" href="#">Add to cart<i class="ps-icon-next"></i></a>
                  <div class="ps-product__actions"><a class="mr-10" href="whishlist.html"><i class="ps-icon-heart"></i></a><a href="compare.html"><i class="ps-icon-share"></i></a></div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="ps-product__content mt-50"></div>
            </div>
          </div>
        </div>
      </div>
      <?php $this->load->view('partials/footer'); ?>  
    </main>
    <?php $this->load->view('partials/scripts'); ?>
</body>
</html>