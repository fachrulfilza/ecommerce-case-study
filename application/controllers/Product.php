<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('product_model');
    }
    
    public function index()
	{
		$data['products'] = $this->product_model->getAllData();

		$this->load->view('products', $data);
	}
    
	public function show($id)
	{
        $data['product'] = $this->product_model->getProductById($id);
        
		$this->load->view('product-detail', $data);
    }
}
